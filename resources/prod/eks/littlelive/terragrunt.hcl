include "root" {
  path = find_in_parent_folders()
}

terraform {
  source = "tfr:///terraform-aws-modules/eks/aws//?version=20.10.0"
}

locals {
  atlantis_terraform_version = "v1.8.3"
  atlantis_workflow          = "terragrunt"
}

inputs = {
  cluster_name                         = "littlelive-prod-eks-1"
  cluster_version                      = "1.29"
  cluster_endpoint_private_access      = true
  cluster_endpoint_public_access       = true
  cluster_enabled_log_types            = ["audit", "api", "authenticator"]
  cluster_endpoint_public_access_cidrs = ["0.0.0.0/0"]
  cluster_encryption_config            = {}

  cluster_ip_family         = "ipv4"
  cluster_service_ipv4_cidr = "10.98.0.0/20"

  enable_cluster_creator_admin_permissions = true

  authentication_mode = "API_AND_CONFIG_MAP"

  vpc_id                   = "vpc-0c405c66b5715145f"
  control_plane_subnet_ids = ["subnet-0d3011facc7310f5c", "subnet-04fc725d4098387c8"]
  subnet_ids               = ["subnet-0d3011facc7310f5c"] // worker nodes subnets
  // pod_subnet_ids           = ["subnet-062a925ca9920d706"] // pods subnets (configured through custom vpc-cni)

  enable_irsa = true

  cluster_addons = {
    coredns = {
      most_recent = true
      configuration_values = jsonencode({
        tolerations = [
          {
            key      = "role"
            operator = "Exists"
          },
        ]
      })
    }
    kube-proxy = {
      most_recent = true
    }
    aws-ebs-csi-driver = {
      most_recent = true
      configuration_values = jsonencode({
        controller = {
          serviceAccount = {
            annotations = {
              "eks.amazonaws.com/role-arn" = "arn:aws:iam::544985268666:role/aws-ebs-csi-driver"
            }
          }
        }
        node = {
          serviceAccount = {
            annotations = {
              "eks.amazonaws.com/role-arn" = "arn:aws:iam::544985268666:role/aws-ebs-csi-driver"
            }
          }
        }
      })
    }
    snapshot-controller = {
      most_recent = true
    }
    vpc-cni = {
      most_recent    = true
      before_compute = true
      configuration_values = jsonencode({
        env = {
          ENABLE_PREFIX_DELEGATION           = "true"
          WARM_PREFIX_TARGET                 = "1"
          DISABLE_METRICS                    = "false"
          AWS_VPC_K8S_CNI_CUSTOM_NETWORK_CFG = "true"
          ENI_CONFIG_LABEL_DEF               = "topology.kubernetes.io/zone"
          AWS_VPC_K8S_CNI_LOGLEVEL           = "debug"
          AWS_VPC_K8S_PLUGIN_LOG_LEVEL       = "debug"
        }
      })
    }
  }

  tags = {
    Terraform   = "true"
    Environment = "prod"
  }

  eks_managed_node_group_defaults = {
    subnet_ids = ["subnet-0d3011facc7310f5c"]
  }

  eks_managed_node_groups = {
    "prod-nodes" = {
      // create                 = false
      use_name_prefix        = true
      ami_type               = "AL2_x86_64"
      instance_types         = ["t3a.medium"]
      disk_size              = 50
      min_size               = 0
      max_size               = 5
      desired_size           = 1
      vpc_security_group_ids = []
      update_config = {
        max_unavailable = 1
      }
      labels = {
        "role" = "prod-nodes"
      }
    }
  }
}