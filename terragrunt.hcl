locals {
  root_path = get_repo_root()

  split_path = split("/", path_relative_to_include())
  count_path = length(local.split_path)
  config_key = join("/", slice(local.split_path, 1, local.count_path))

  env_path = local.split_path[1]
}

inputs = {
  config_key = local.config_key
  root_path  = local.root_path
  count_path = local.count_path
  env_vars   = local.env_path
}

generate "provider" {
  path      = "provider.tf"
  if_exists = "skip"
  contents  = <<EOF
  provider "aws" {
    region = "us-east-1"
  }
  EOF
}

remote_state {
  backend = "s3"
  generate = {
    path      = "backend.tf"
    if_exists = "skip"
  }
  config = {
    bucket = "littlelive-terraform-state"
    key    = "${local.config_key}/terraform.tfstate"
    region = "us-east-1"
  }
}