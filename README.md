# terragunt-littlelive

prequisite on local windows
========================

- install aws sdk
- Install Terraform and Terragrunt.
- install git
- install eksctl
- install kubectl




How to run Terragrunt to setup the environment
==========================================
terragrunt plan
terragrunt apply


for VPC and RDS setup
======================
It's still being created manually by UI Console AWS.

Create ECR by command
====================
aws ecr create-repository --repository-name realworld-nodejs-app



to destroy the environment
========================
terragrunt destroy


Improvement
============
- Create a VPC by Terragrunt.
- Create RDS by Terragrunt with multi-AZ
- Create ECR by Terragrunt


Best Practice
==============
- Create two subnets: subnet private and subnet public.
- implement WAF
- Create different environments for development, QA, performance testing, and production.
- have segmentation on the network for development, QA, performance, and production
- Implement a security group.
- use least privilege on IAM
- Use the IAM Group for adding permission instead of adding permission at the user level.
- Use OpenVPN to access our cluster.
- have a private repository for our code and image
- High availability and fault tolerance
- Have centralized monitoring infrastructure and applications for alerting and logging.
- Deploy in three availability zones to avoid downtime.
- Review resource utilization every 2 months to determine which resource is overprovisioning and save costs.
- review IAM every 4 months to review privillage 
- Active MFA
- rotate the password every 3 months.
- have a 15-length password with the complexity of upper case, lower case, and symbol combination



Documentation
================
- https://docs.gitlab.com/ee/user/project/clusters/
- https://docs.gitlab.com/ee/ci/variables/
- https://terragrunt.gruntwork.io/docs/
- https://docs.aws.amazon.com/rekognition/latest/dg/setup-awscli-sdk.html
- https://developer.hashicorp.com/terraform/tutorials/aws-get-started/install-cli
- https://terragrunt.gruntwork.io/docs/getting-started/install/
- https://kubernetes.io/docs/tasks/tools/install-kubectl-windows/
- https://www.git-scm.com/downloads
- https://eksctl.io/installation/
